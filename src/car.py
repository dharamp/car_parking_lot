
class Car(object):
    """
        Class having constructor, decorators, setter and getters for car
    """

    def __init__(self):
        self._reg_no = None
        self._colour = None

    @classmethod
    def create(self, reg_no, colour):
        obj = self()
        obj.reg_no = reg_no
        obj.colour = colour
        return obj
