import traceback
import sys, os
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/../src")

import parking_slot, car

class Parking(object):
    """ This class have the methods """

    def __init__(self):
        self.slots = {}

    def create_parking_lot(self, total_slots):
        """ Methods to create parking lot for given.
            param: no_of_slots : Integer """
        try:
            total_slots = int(total_slots)
        except ValueError:
            print("Number of slots is incorrect: "+str(total_slots))
            return

        if len(self.slots) > 0:
            print("Parking Lot already created")
            return

        if total_slots > 0:
            for i in range(1, total_slots+1):
                temp_slot = parking_slot.ParkingSlot(parking_no=i, available=True)
                self.slots[i] = temp_slot

            print("Created a parking slot with {} slots".format(total_slots))
        else:
            print("Number of slots provided is incorrect.")
        return

    def get_available_slot(self):
        """ To get available slot in an order """
        try:
            # Filter available slots. Syntax filter(function, iterable)
            slots_available = filter(lambda x: x.available, self.slots.values())

            if not slots_available:
                return None

            # Sort the available slots. Syntax sorted(iterable[, key][, reverse])
            slots = sorted(slots_available, key=lambda x: x.parking_no)
            return slots
        except:
            print("Exeception in get_available_slot method: "+str(traceback.format_exc()))


    def park(self, reg_no=None, colour=None):
        """Method will assign the parking lot of available otherwise print message.
            Params:   reg_no - String Type
                                colour - String Type
                                                        """
        try:
            if reg_no is None or colour is None:
                print("Paramter Missing")
                return

            if not self._slot_exists():
                return

            available_slot = self.get_available_slot()
            if len(available_slot)>0:
                # create car object and assign the available slot
                available_slot[0].car = car.Car.create(reg_no, colour)

                # Mark slot available as False
                available_slot[0].available = False
                print("Allocated slot number: "+str(available_slot[0].parking_no))
            else:
                print("Sorry, parking lot is full")
        except:
            print("Exeception in park method: "+str(traceback.format_exc()))

    def leave(self, parking_no=None):
        """ Methods to frees the given slot no in param  """
        try:

            if parking_no is None:
                print("Paramter Missing")
                return

            parking_no = int(parking_no)
            if not self._slot_exists():
                return

            if parking_no in self.slots:
                assigned_lot = self.slots[parking_no]
                if not assigned_lot.available and assigned_lot.car:
                    assigned_lot.car = None
                    assigned_lot.available = True
                    print("Slot number {} is free".format(parking_no))
                else:
                    print("No car is present at slot number " +str(parking_no))
            else:
                print("Sorry, slot number does not exist in parking lot.")
        except:
            print("Exeception in leave method: "+str(traceback.format_exc()))

    def status(self):
        """ Display the status of parking lots with car details """
        try:
            if not self._slot_exists():
                return

            print("Slot No. Registration No Colour")
            for i in self.slots.values():
                if not i.available and i.car:
                    print(i.parking_no, i.car.reg_no, i.car.colour)
        except:
            print("Exeception in status method: "+str(traceback.format_exc()))

    def _slot_exists(self):
        if len(self.slots) == 0:
            print("Parking lot not created.")
            return False
        return True

    def registration_numbers_for_cars_with_colour(self, colour=None):
        """ Get car detail from car colour """
        try:
            if colour is None:
                print("Paramter Missing")
                return

            if not self._slot_exists():
                return

            reg_nos = ''
            for assigned_lot in self.slots.values():
                if not assigned_lot.available and assigned_lot.car and assigned_lot.car.colour == colour:
                    reg_nos += '%s ' % assigned_lot.car.reg_no

            if reg_nos:
                reg_nos = reg_nos.strip().replace(" ", ", ")
                print(reg_nos)
            else:
                print("None Found")
        except:
            print("Exeception in registration_numbers_for_cars_with_colour method: "+str(traceback.format_exc()))

    def slot_numbers_for_cars_with_colour(self, colour=None):
        """ Get Car lot number by their colour
            param: colour - String Type
        """
        try:

            if colour is None:
                print("Paramter Missing")
                return

            if not self._slot_exists():
                return

            parking_nos = ''
            for assigned_lot in self.slots.values():
                if not assigned_lot.available and assigned_lot.car and assigned_lot.car.colour == colour:
                    parking_nos += '%s ' % assigned_lot.parking_no

            if parking_nos:
                parking_nos = parking_nos.strip().replace(" ", ", ")
                print(parking_nos)
            else:
                print("None Found")
        except:
            print("Exeception in slot_numbers_for_cars_with_colour method: "+str(traceback.format_exc()))


    def slot_number_for_registration_number(self, reg_no=None):
        """ Get Slot number by car registration number """
        try:

            if reg_no is None:
                print("Paramter Missing")
                return

            if not self._slot_exists():
                return

            parking_no = ''
            for assigned_lot in self.slots.values():
                if not assigned_lot.available and assigned_lot.car and assigned_lot.car.reg_no == reg_no:
                    parking_no = assigned_lot.parking_no
                    break

            if parking_no:
                print(parking_no)
            else:
                print("None found")
        except:
            print("Exeception in slot_number_for_registration_number method: "+str(traceback.format_exc()))


