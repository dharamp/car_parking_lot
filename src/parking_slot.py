
class ParkingSlot(object):
    """
        Class having constructor, decorators, setter and getters for slot
    """

    def __init__(self, parking_no=None, available=True):
        self.car = None
        self.parking_no = parking_no
        self.available = available

