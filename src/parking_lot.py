import os, sys
import parking
import traceback

class ParkingCommands(object):

    def __init__(self):
        self.parking = parking.Parking()

    def process_input_file(self, input_file):
        if not os.path.exists(input_file):
            print("Given file %s does not exist" +str(input_file))
        file_obj = open(input_file)
        try:
            while True:
                line = file_obj.readline()
                if line.endswith('\n'): line = line[:-1]
                if line == '': continue
                self.process_command(line)
        except StopIteration:
            file_obj.close()
        except:
            print("Exeception in process_file method: "+str(traceback.format_exc()))

    def process_input(self):
        try:
            while True:
                stdin_input = input("Enter command: ")
                self.process_command(stdin_input)
        except (KeyboardInterrupt, SystemExit):
            return
        except:
            print("Exiting.")


    def process_command(self, stdin_input):
        try:
            inputs = stdin_input.split()
            command = inputs[0]
            params = inputs[1:]
            #print(command, params)
            if hasattr(self.parking, command):
                command_function = getattr(self.parking, command)
                command_function(*params)
            else:
                print("Wrong command.")
        except:
            print("Exeception in process_command method: "+str(traceback.format_exc()))


if __name__ == "__main__":
    args = sys.argv
    if len(args) == 1:
        pk_command = ParkingCommands()
        pk_command.process_input()
    elif len(args) == 2:
        pk_command = ParkingCommands()
        pk_command.process_input_file(args[1])
    else:
        print("Invalid  arguments.")

