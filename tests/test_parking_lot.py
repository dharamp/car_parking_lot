import unittest
import sys
import os
from src import parking

class TestParkingLot(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.parking = parking.Parking()
        self.allocated_slot = 1

    # Test to create parking slot with invalid slot no
    def test_for_create_parking_lot_with_invalid_slot_no(self):
        parking_slots = "7a"
        dd= self.parking.create_parking_lot(parking_slots)
        self.assertEqual(len(self.parking.slots), 0)

    # Test to create parking slot with valid slot no
    def test_for_create_parking_lot_with_valid_slot_no(self):
        parking_slots = 6
        self.parking.create_parking_lot(parking_slots)
        self.assertEqual(len(self.parking.slots), parking_slots)

    # Test to park the car in parking
    def test_for_park_car(self):
        reg_no = "KA-01-HH-2701"
        colour = "Blue"
        self.parking.park(reg_no, colour)
        for i in self.parking.slots.values():
            if not i.available and i.car:
                self.assertEqual(i.car.reg_no, reg_no)
                self.assertEqual(i.car.colour, colour)

    # Test to get Registration number from vehicle colour
    def test_for_reg_no_for_cars_with_colour(self):
        reg_no = "KA-01-HH-2701"
        colour = "Blue"
        self.parking.registration_numbers_for_cars_with_colour(colour)
        for i in self.parking.slots.values():
            if not i.available and i.car:
                self.assertEqual(i.car.reg_no, reg_no)

    # Test to get parking slot number from vehicle colour
    def test_for_slot_numbers_for_cars_with_colour(self):
        reg_no = "KA-01-HH-2701"
        colour = "Blue"
        self.parking.slot_numbers_for_cars_with_colour(colour)
        for i in self.parking.slots.values():
            if not i.available and i.car:
                self.assertEqual(i.parking_no, self.allocated_slot)


    # Test to get Slot number from Vehicle registration number
    def test_for_slot_number_for_registration_number(self):
        reg_no = "KA-01-HH-2701"
        colour = "Blue"
        self.parking.slot_number_for_registration_number(reg_no)
        for i in self.parking.slots.values():
            if not i.available and i.car:
                self.assertEqual(i.parking_no, self.allocated_slot)


    ## Test to pick the car from the parking
    def test_for_unpark_car(self):
        self.parking.leave(self.allocated_slot)
        self.assertTrue(self.parking.slots[self.allocated_slot].available, "Leave failed.")

    @classmethod
    def tearDownClass(self):
        del self.parking

if __name__ == '__main__':
    unittest.main()


