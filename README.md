# Car Parking lot

## Run the application

* Python 3.7.4 or 3+ [Setup file will install latest python on MaC]
* `$ ./bin/setup`

* Change the file permissions `chmod +x parking_lot`

* Executables files in /bin

* Execute the application

## Run App

* `$ ./bin/parking_lot`

## Run tests

* `$ python -m unittest tests.test_parking_lot'

* OR

* `$ ./bin/test_parking_lot`
